package com.example.demogcp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemogcpApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemogcpApplication.class, args);
	}

}

