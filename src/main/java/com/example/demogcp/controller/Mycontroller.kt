package com.example.demogcp.controller

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class Mycontroller{

    @RequestMapping("/helloworld")
    fun helloWorld(@RequestParam name:String):String{

        var msg:String = name+" Hello World"
        return msg
    }

}